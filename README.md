# Assume-Role-Ansible
This is a repo of example code to perform ansible tasks with assumed roles.

Items in the aws directory are to be placed in the .aws folder in the homedir of the user/account running the ansible tasks. All other items can exist in a working directory.

Command to run the playbook : ansible-playbook test.yml -vvv

Requirements:

Ansible 2.2.0
Pre-configured AWS accounts
Aws Cli
IAM Role with Custom Policies attached

